# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2014-02-13 21:17+0100\n"
"PO-Revision-Date: 2014-01-12 13:26+0100\n"
"Last-Translator: intrigeri <intrigeri@boum.org>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Call for testing: 0.22.1~rc1\"]] [[!meta date=\"2014-01-11 00:18:00\"]]"
msgid "[[!meta title=\"Call for testing: 0.22.1~rc1\"]]\n"
msgstr "[[!meta title=\"Appel à tester Tails 0.22.1~rc1\"]] [[!meta date=\"2014-01-11 00:18:00\"]]"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "[[!meta title=\"Call for testing: 0.22.1~rc1\"]] [[!meta date=\"2014-01-11 00:18:00\"]]"
msgid "[[!meta date=\"2014-01-11 00:18:00\"]]\n"
msgstr "[[!meta title=\"Appel à tester Tails 0.22.1~rc1\"]] [[!meta date=\"2014-01-11 00:18:00\"]]"

#. type: Plain text
msgid ""
"You can help Tails! The first release candidate for the upcoming version "
"0.22.1 is out. Please test it and see if it works for you."
msgstr ""
"Vous pouvez aider Tails ! La première version candidate pour Tails 0.22.1 "
"est sortie. Merci de la tester et de voir si tout fonctionne pour vous."

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr ""

#. type: Title =
#, fuzzy, no-wrap
#| msgid "How to test Tails 0.22.1~rc1?"
msgid "How to test Tails 0.22.1~rc1?\n"
msgstr "Comment tester Tails 0.22.1~rc1 ?"

#. type: Bullet: '1. '
#, fuzzy
#| msgid ""
#| "<strong>Keep in mind that this is a test image.</strong> We have made "
#| "sure that it is not broken in an obvious way, but it might still contain "
#| "undiscovered issues."
msgid ""
"**Keep in mind that this is a test image.** We have made sure that it is not "
"broken in an obvious way, but it might still contain undiscovered issues."
msgstr ""
"<strong>Gardez à l'esprit que c'est une image de test.</strong> Nous nous "
"sommes assurés qu'elle n'est pas corrompue d'une manière évidente, mais elle "
"peut toujours contenir des problèmes non découverts."

#. type: Bullet: '2. '
msgid ""
"Either try the <a href=\"#automatic_upgrade\">automatic upgrade</a>, or "
"download the ISO image and its signature:"
msgstr ""
"Vous pouvez soit essayer la <a href=\"#automatic_upgrade\">mise à jour "
"automatique</a>, soit télécharger l'image ISO et sa signature :"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-0.22.1~rc1/tails-i386-0.22.1~rc1.iso\" >Tails 0.22.1~rc1 ISO image</a>"
msgid "   <a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-0.22.1~rc1/tails-i386-0.22.1~rc1.iso\" >Tails 0.22.1~rc1 ISO image</a>\n"
msgstr "<a class=\"download-file\" href=\"http://dl.amnesia.boum.org/tails/alpha/tails-i386-0.22.1~rc1/tails-i386-0.22.1~rc1.iso\" >Image ISO de Tails 0.22.1~rc1</a>"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a class=\"download-signature\" href=\"https://tails.boum.org/torrents/files/tails-i386-0.22.1~rc1.iso.sig \" >Tails 0.22.1~rc1 signature</a>"
msgid ""
"   <a class=\"download-signature\"\n"
"   href=\"https://tails.boum.org/torrents/files/tails-i386-0.22.1~rc1.iso.sig\">Tails 0.22.1~rc1 signature</a>\n"
msgstr "<a class=\"download-signature\" href=\"https://tails.boum.org/torrents/files/tails-i386-0.22.1~rc1.iso.sig \" >Signature de Tails 0.22.1~rc1</a>"

#. type: Bullet: '1. '
msgid "[[Verify the ISO image|download#verify]]."
msgstr "[[Vérifiez l'image ISO|download#verify]]."

#. type: Bullet: '1. '
msgid ""
"Have a look at the list of <a href=\"#known_issues\">known issues of this "
"release</a> and the list of [[longstanding known issues|support/"
"known_issues]]."
msgstr ""
"Jetez un œil à la liste des <a href=\"#known_issues\">problèmes connus de "
"cette version</a> et à la liste des [[problèmes connus de longue date|"
"support/known_issues]]."

#. type: Bullet: '1. '
msgid "Test wildly!"
msgstr "Testez à volonté !"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "If you find anything that is not working as it should, please [[report to us|doc/first_steps/bug_reporting]]! Bonus points if you check that it is not a <a href=\"#known_issues\">known issue of this release</a> or a [[longstanding known issue|support/known_issues]]."
msgid ""
"If you find anything that is not working as it should, please [[report to\n"
"us|doc/first_steps/bug_reporting]]! Bonus points if you check that it is not a\n"
"<a href=\"#known_issues\">known issue of this release</a> or a\n"
"[[longstanding known issue|support/known_issues]].\n"
msgstr "Si vous découvrez quelque chose qui ne fonctionne pas comme prévu, merci de [[nous le rapporter|doc/first_steps/bug_reporting]] ! Points bonus si vous vérifiez que ce n'est pas un <a href=\"#known_issues\">problème connu de cette version</a> ou un [[problème connu de longue date|support/known_issues]]."

#. type: Plain text
#, no-wrap
msgid "<div id=\"automatic_upgrade\"></a>\n"
msgstr ""

#. type: Title =
#, fuzzy, no-wrap
#| msgid "<a id=\"automatic_upgrade\"></a>How automatically upgrade from 0.22?"
msgid "How automatically upgrade from 0.22?\n"
msgstr "<a id=\"automatic_upgrade\"></a>Comment mettre à jour automatiquement depuis la version 0.22 ?"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "These steps allow you to automatically upgrade a device installed with <span class=\"application\">Tails Installer</span> from Tails 0.22 to Tails 0.22.1~rc1."
msgid ""
"These steps allow you to automatically upgrade a device installed with <span\n"
"class=\"application\">Tails Installer</span> from Tails 0.22 to Tails 0.22.1~rc1.\n"
msgstr "Ces étapes vous permettent de mettre à jour automatiquement votre périphérique, installé avec l'<span class=\"application\">Installeur de Tails</span>, depuis Tails 0.22 vers Tails 0.22.1~rc1."

#. type: Bullet: '1. '
msgid ""
"Start Tails 0.22 and [[set an administration password|doc/first_steps/"
"startup_options/administration_password]]."
msgstr ""
"Démarrer Tails 0.22 et [[définir un mot de passe d'administration|doc/"
"first_steps/startup_options/administration_password]]."

#. type: Bullet: '2. '
#, fuzzy
#| msgid ""
#| "Run this command in a <span class=\"application\">Root Terminal</span> to "
#| "select the \"alpha\" upgrade channel:"
msgid ""
"Run this command in a <span class=\"application\">Root Terminal</span> to "
"select the \"alpha\" upgrade channel: <pre><code>echo TAILS_CHANNEL=\\\"alpha"
"\\\" >> /etc/os-release</code></pre>"
msgstr ""
"Lancez cette commande dans un <span class=\"application\">Terminal "
"administrateur</span> pour sélectionner le canal de mise à jour \"alpha\" :"

#. type: Bullet: '3. '
msgid ""
"Run this command in a <span class=\"application\">Root Terminal</span> to "
"install the latest version of <span class=\"application\">Tails Upgrader</"
"span>:"
msgstr ""
"Lancez cette commande dans un <span class=\"application\">Terminal "
"administrateur</span> pour installer la dernière version de <span class="
"\"application\">Tails Upgrader</span> :"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid ""
#| "<code>echo \"deb http://deb.tails.boum.org/ 0.22.1-rc1 main\" \\\n"
#| "    > /etc/apt/sources.list.d/tails-0.22-rc1.list &amp;&amp; \\\n"
#| "    sed -i -e &#39;/tor-0.2.4.x-squeeze/ d&#39; \\\n"
#| "   /etc/apt/sources.list.d/torproject.list &amp;&amp;\n"
#| "    apt-get update &amp;&amp; \\\n"
#| "    apt-get install tails-iuk</code>"
msgid ""
"       echo \"deb http://deb.tails.boum.org/ 0.22.1-rc1 main\" \\\n"
"           > /etc/apt/sources.list.d/tails-0.22-rc1.list && \\\n"
"           sed -i -e '/tor-0.2.4.x-squeeze/ d' \\\n"
"          /etc/apt/sources.list.d/torproject.list &&\n"
"           apt-get update && \\\n"
"           apt-get install tails-iuk\n"
msgstr ""
"<code>echo \"deb http://deb.tails.boum.org/ 0.22.1-rc1 main\" \\\n"
"    > /etc/apt/sources.list.d/tails-0.22-rc1.list &amp;&amp; \\\n"
"    sed -i -e &#39;/tor-0.2.4.x-squeeze/ d&#39; \\\n"
"   /etc/apt/sources.list.d/torproject.list &amp;&amp;\n"
"    apt-get update &amp;&amp; \\\n"
"    apt-get install tails-iuk</code>"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</span>&nbsp;&#x25B8; <span class=\"guisubmenu\">Tails</span>&nbsp;&#x25B8; <span class=\"guimenuitem\">Tails Upgrader</span> </span> to start <span class=\"application\">Tails Upgrader</span>."
msgid ""
"4. Choose <span class=\"menuchoice\">\n"
"    <span class=\"guimenu\">Applications</span>&nbsp;&#x25B8;\n"
"    <span class=\"guisubmenu\">Tails</span>&nbsp;&#x25B8;\n"
"    <span class=\"guimenuitem\">Tails Upgrader</span>\n"
"  </span>\n"
"  to start <span class=\"application\">Tails Upgrader</span>.\n"
msgstr "Choisissez <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</span>&nbsp;&#x25B8; <span class=\"guisubmenu\">Tails</span>&nbsp;&#x25B8; <span class=\"guimenuitem\">Tails Upgrader</span> </span> pour lancer <span class=\"application\">Tails Upgrader</span>."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Accept <span class=\"application\">Tails Upgrader</span>&#39;s proposal to upgrade to Tails 0.22.1~rc1."
msgid ""
"5. Accept <span class=\"application\">Tails Upgrader</span>&#39;s proposal\n"
"to upgrade to Tails 0.22.1~rc1.\n"
msgstr "Acceptez la proposition de <span class=\"application\">Tails Upgrader</span> de mettre à jour vers Tails 0.22.1~rc1."

#. type: Bullet: '6. '
msgid "Wait while the upgrade is downloaded and applied."
msgstr "Attendez pendant que la mise à jour est téléchargée et appliquée."

#. type: Bullet: '7. '
msgid "Restart the system when advised to do so."
msgstr "Redémarrez le système lorsque cela vous est proposé."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Once the system is restarted, choose <span class=\"menuchoice\"> <span class=\"guimenu\">System</span>&nbsp;&#x25B8; <span class=\"guimenuitem\">About Tails</span> </span> to confirm that the running system is now Tails 0.22.1~rc1."
msgid ""
"8. Once the system is restarted, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">System</span>&nbsp;&#x25B8;\n"
"  <span class=\"guimenuitem\">About Tails</span>\n"
"</span>\n"
"to confirm that the running system is now Tails 0.22.1~rc1.\n"
msgstr "Une fois le système redémarré, choisissez <span class=\"menuchoice\"> <span class=\"guimenu\">Système</span>&nbsp;&#x25B8; <span class=\"guimenuitem\">À propos de Tails</span> </span> pour vérifier que vous utilisez bien Tails 0.22.1~rc1 désormais."

#. type: Title =
#, fuzzy, no-wrap
#| msgid "What's new since 0.22?"
msgid "What's new since 0.22?\n"
msgstr "Quoi de neuf depuis la 0.22 ?"

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Update NSS to 3.14.5-1~bpo60+1."
msgid ""
"- Security fixes\n"
"  - Update NSS to 3.14.5-1~bpo60+1.\n"
msgstr "Mise à jour de NSS vers la version 3.14.5-1~bpo60+1."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "Check for upgrades availability using Tails Upgrader, and propose to apply an incremental upgrade whenever possible."
msgid ""
"- Major improvements\n"
"  - Check for upgrades availability using Tails Upgrader, and propose to apply an incremental upgrade whenever possible.\n"
"  - Install Linux 3.12 (3.12.6-2).\n"
msgstr "Vérifie les mises à jour disponible avec le Tails Upgrader, et propose d'appliquer les mises à jour de manière incrémentale quand cela est possible."

#. type: Plain text
#, no-wrap
msgid ""
"- Bugfixes\n"
"  - Fix the keybindings problem introduced in 0.22.\n"
"  - Fix the Unsafe Browser problem introduced in 0.22.\n"
"  - Use IE's icon in Windows camouflage mode.\n"
"  - Handle some corner cases better in Tails Installer.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"- Minor improvements\n"
"  - Update Tor Browser to 24.2.0esr-1+tails1.\n"
"  - Update Torbutton to 1.6.5.3.\n"
"  - Do not start Tor Browser automatically, but notify when Tor is ready.\n"
"  - Import latest Tor Browser prefs.\n"
"  - Many user interface improvements in Tails Upgrader.\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "See the <a href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">online Changelog</a> for technical details."
msgid ""
"See the <a href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">online\n"
"Changelog</a> for technical details.\n"
msgstr "Voir le <a href=\"https://git-tails.immerda.ch/tails/plain/debian/changelog?h=stable\">journal des modifications</a> pour les détails techniques."

#. type: Plain text
#, fuzzy, no-wrap
#| msgid "<a id=\"known_issues\"></a>Known issues in 0.22.1~rc1"
msgid "<a id=\"known_issues\"></a>\n"
msgstr "<a id=\"known_issues\"></a>Problèmes connus dans la version 0.22.1~rc1"

#. type: Title =
#, fuzzy, no-wrap
#| msgid "<a id=\"known_issues\"></a>Known issues in 0.22.1~rc1"
msgid "Known issues in 0.22.1~rc1\n"
msgstr "<a id=\"known_issues\"></a>Problèmes connus dans la version 0.22.1~rc1"

#. type: Bullet: '- '
msgid ""
"The memory erasure on shutdown [[!tails_ticket 6460 desc=\"does not work on "
"some hardware\"]]."
msgstr ""
"L'effacement de la mémoire à l'extinction [[!tails_ticket 6460 desc=\"ne "
"fonctionne pas sur certains matériels\"]]."

#~ msgid "<code>echo TAILS_CHANNEL=\\\"alpha\\\" >> /etc/os-release</code>"
#~ msgstr "<code>echo TAILS_CHANNEL=\\\"alpha\\\" >> /etc/os-release</code>"

#~ msgid "Security fixes"
#~ msgstr "Mise à jour de sécurité"

#~ msgid "Major improvements"
#~ msgstr "Améliorations majeures"

#~ msgid "Install Linux 3.12 (3.12.6-2)."
#~ msgstr "Installation de Linux 3.12 (3.12.6-2)."

#~ msgid "Bugfixes"
#~ msgstr "Corrections de bugs"

#~ msgid "Fix the keybindings problem introduced in 0.22."
#~ msgstr "Correction du problème de raccourcis introduit dans la 0.22."

#~ msgid "Fix the Unsafe Browser problem introduced in 0.22."
#~ msgstr ""
#~ "Correction du problème du Navigateur Non-sécurisé introduit dans la 0.22."

#~ msgid "Use IE's icon in Windows camouflage mode."
#~ msgstr "Utilise l'icône d'IE en mode Windows camouflage."

#~ msgid "Handle some corner cases better in Tails Installer."
#~ msgstr ""
#~ "Meilleure prise en charge de certains cas particuliers par l'Installeur "
#~ "de Tails."

#~ msgid "Minor improvements"
#~ msgstr "Améliorations mineures"

#~ msgid "Update Tor Browser to 24.2.0esr-1+tails1."
#~ msgstr "Mise à jour du Tor Browser vers la version 24.2.0esr-1+tails1."

#~ msgid "Update Torbutton to 1.6.5.3."
#~ msgstr "Mise à jour de Torbutton vers la version 1.6.5.3."

#~ msgid ""
#~ "Do not start Tor Browser automatically, but notify when Tor is ready."
#~ msgstr ""
#~ "Le Tor Browser ne se lance plus automatiquement, mais l'utilisateur est "
#~ "avertit quand Tor est prêt."

#~ msgid "Import latest Tor Browser prefs."
#~ msgstr "Importation des dernières préférences du Tor Browser."

#~ msgid "Many user interface improvements in Tails Upgrader."
#~ msgstr ""
#~ "Nombre d'améliorations de l'interface utilisateur dans Tails Upgrader."
