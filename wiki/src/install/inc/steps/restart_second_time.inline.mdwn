Restart on the <span class="usb">final</span><span class="clone">new</span> Tails<span class="dvd"> USB stick</span>
====================================================================================================================

1. Shut down the computer.

   <div class="step-image">
   [[!img inc/infography/restart-on-final-tails.png link="no" class="usb clone"]]
   [[!img inc/infography/restart-on-tails-usb.png link="no" class="dvd"]]
   </div>

1. <p class="usb clone">Unplug the <span class="usb">first</span><span class="clone">other</span> USB stick and leave the <span class="usb">second</span><span class="clone">new</span> USB stick plugged in.</p>
   <p class="dvd">Eject the DVD and leave the USB stick plugged in.</p>

1. Switch on the computer.

   [[!inline pages="install/inc/steps/mac_startup_disks.inline" raw="yes"]]

   <div class="note windows-usb linux-usb">
   <p>Some computers start successfully on the temporary Tails but fail
   to start on the final Tails. If this is your case, refer to the
   troubleshooting section about [[!toggle id="not_at_all" text="Tails
   not starting at all"]].</p>
   </div>

   [[!toggleable id="not_at_all" text="""
   <span class="hide">[[!toggle id="not_at_all" text=""]]</span>

   [[!inline pages="install/inc/steps/not_at_all.inline" raw="yes"]]

   """]]

   <div class="bug mac-usb mac-dvd">
   <p>If your computer fails to start on the <span class="usb">final Tails,</span><span class="dvd">Tails USB stick,</span> you can
   continue using the <span class="usb">temporary Tails</span><span class="dvd">Tails DVD</span>. <span class="usb">On this temporary Tails you
   cannot benefit from automatic upgrades or create an encrypted
   persistent storage.</span></p>
   <p>You should still have a look at our
   <span class="mac-usb">[[final recommendations|mac/usb#recommendations]].</span>
   <span class="mac-dvd">[[final recommendations|mac/dvd#recommendations]].</span></p>
   </div>

1. In the **Boot Tails** menu, choose **Live** and press **Enter**.

1. After 30&ndash;60 seconds, *Tails Greeter* appears.

1. In *Tails Greeter*, select your preferred language in the drop-down
list on the bottom left of the screen. Click **Login**.

1. After 15&ndash;30 seconds, the Tails desktop appears.
