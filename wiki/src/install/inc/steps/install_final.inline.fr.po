# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2015-11-20 08:54+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title =
#, no-wrap
msgid "<span class=\"debian windows linux mac expert install-clone\">Install <span class=\"windows-usb linux-usb mac-usb\">the final</span> Tails<span class=\"dvd\"> on a USB stick</span></span><span class=\"upgrade\">Upgrade your Tails</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p class=\"debian windows linux mac expert\">In this step, you will install <span class=\"windows-usb linux-usb mac-usb\">the final</span> Tails\n"
"on a <span class=\"windows-usb linux-usb mac-usb\">second</span><span class=\"install-clone mac-clone\">new</span> USB stick\n"
"using <span class=\"application\">Tails Installer</span>.</p>\n"
"<p class=\"upgrade-clone\">In this step, you will upgrade your Tails from the other Tails using <span class=\"application\">Tails Installer</span>.</p>\n"
"<p class=\"upgrade-tails\">In this step, you will upgrade your Tails from the temporary Tails using <span class=\"application\">Tails Installer</span> one last time.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution debian windows linux mac\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>All the data on this USB stick will be lost.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"note upgrade\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>The persistent storage of your Tails USB stick will be preserved.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p class=\"upgrade-clone\">The persistent storage of the other Tails will not be copied.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img inc/infography/plug-usb.png link=\"no\" class=\"debian-usb\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img inc/infography/plug-second-usb.png link=\"no\" class=\"windows-usb linux-usb mac-usb install-clone mac-clone\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img inc/infography/plug-tails-usb.png link=\"no\" class=\"dvd\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img inc/infography/plug-upgraded-usb.png link=\"no\" class=\"upgrade-clone upgrade-tails\"]]\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Plug <span class=\"windows-usb linux-usb mac-usb\">the second</span><span "
"class=\"install-clone mac-clone\">the new</span><span class=\"upgrade\">your "
"Tails</span><span class=\"debian-usb expert dvd\">the</span> USB stick in "
"the computer."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"1. <div class=\"windows linux mac upgrade install-clone\"><p>\n"
"   Choose\n"
"   <span class=\"menuchoice\">\n"
"     <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"     <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"     <span class=\"guimenuitem\">Tails Installer</span>\n"
"   </span>\n"
"   to start <span class=\"application\">Tails Installer</span>.\n"
"   </p></div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"debian expert\"><p>\n"
"   Start <span class=\"application\">Tails Installer</span>.\n"
"   </p></div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Click on the <span class=\"button windows linux mac install-clone\">Install "
"by cloning</span> <span class=\"button debian expert\">Install</span> <span "
"class=\"button upgrade\">Upgrade by cloning</span> button."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img inc/screenshots/tails_installer_in_tails.png link=no class=\"screenshot windows linux mac upgrade install-clone\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img inc/screenshots/tails_installer_in_debian.png link=no class=\"screenshot debian expert\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <div class=\"step-image\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img inc/infography/install-tails.png link=\"no\" class=\"debian-usb\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img inc/infography/clone-temporary-tails.png link=\"no\" class=\"windows-usb linux-usb mac-usb clone upgrade-tails\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   [[!img inc/infography/clone-dvd.png link=\"no\" class=\"dvd\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   </div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"<p class=\"debian\">Click <span class=\"button\">Browse</span> and choose "
"the ISO image that you downloaded earlier.</p>"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   Choose\n"
"   <span class=\"windows-usb linux-usb mac-usb\">the second</span>\n"
"   <span class=\"install-clone mac-clone\">the new</span>\n"
"   <span class=\"debian-usb expert dvd upgrade-clone\">your</span>\n"
"   USB stick in the <span class=\"guilabel\">Target Device</span> drop-down list.\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"To start the <span class=\"debian expert windows linux mac install-clone"
"\">installation</span><span class=\"upgrade\">upgrade</span>, click on the "
"<span class=\"button\">Install Tails</span> button."
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Read the warning message in the pop-up window. Click <span class=\"button"
"\">Yes</span> to confirm."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   The <span class=\"debian expert windows linux mac install-clone\">installation</span><span class=\"upgrade\">upgrade</span> takes a few minutes.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"   <div class=\"bug\">\n"
"   <p>The progress bar usually freezes for some time around 95 percent\n"
"   while synchronizing data on disk.</p>\n"
"   </div>\n"
msgstr ""

#. type: Bullet: '1. '
msgid "Close <span class=\"application\">Tails Installer</span>."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"trophy upgrade\">\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>You are done upgrading Tails. You can now shutdown and restart on your Tails USB stick.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>Thank you for staying safe!</p>\n"
msgstr ""
