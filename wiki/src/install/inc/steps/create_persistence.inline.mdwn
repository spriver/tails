Create an encrypted persistent storage (optional)
=================================================

<div class="step-image">[[!img inc/infography/create-persistence.png link="no"]]</div>

You can optionally create an *encrypted persistent storage* in the
remaining free space on the <span class="windows-usb linux-usb mac-usb">final</span><span class="clone">new</span> Tails USB stick to store any of the
following:

  - Your personal files and working documents
  - Some of your settings
  - Your encryption keys

The data in the encrypted persistent storage:

  - Remains available across separate working sessions.
  - Is encrypted using a passphrase of your choice.

Once the encrypted persistent storage is created, you can choose whether
to activate it or not each time you start Tails.

<div class="caution">

<p>The encrypted persistent storage is not hidden. An attacker in possession of
the USB stick can know whether it has an encrypted persistent storage. Take into consideration
that you can be forced or tricked to give out its passphrase.</p>

</div>

<div class="caution">

<p>It is possible to
open the encrypted persistent storage from other operating systems, but it might break
your security.
Other operating systems should probably not be trusted to handle
sensitive information or leave no trace.</p>

</div>

1. Choose
   <span class="menuchoice">
     <span class="guimenu">Applications</span>&nbsp;▸
     <span class="guisubmenu">Tails</span>&nbsp;▸
     <span class="guimenuitem">Configure persistent volume</span></span> to
   start the *Persistence Assistant*. The assistant proposes to create
   an encrypted persistent volume on the USB stick.

1. Specify a passphrase of your choice in both the
<span class="guilabel">Passphrase</span> and <span class="guilabel">Verify
Passphrase</span> text boxes. Click on the <span class="guilabel">Create</span> button.

   <div class="tip">
   <p>We recommend choosing a long passphrase made of several random words
   rather than a single password. For example, "<em>bank violin thread duck knob
   train</em>" is a very strong passphrase.</p>

   <p>To learn how to choose a good passphrase, you can read
   <a href="https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/">this
   article from <em>The Intercept</em></a>.</p>
   </div>

3. Wait for the creation to finish.

   <div class="bug">
   <p>If the creation is interrupted before it finishes, you might not
   be able to start Tails from this USB stick anymore. This can happen if you
   close the window of the assistant or unplug the USB stick during the creation of
   the encrypted persistent storage. Reinstall Tails on this USB stick to fix this issue.</p>
   </div>

4. The assistant shows a list of the possible persistence features. Each
   feature corresponds to a set of files or settings to be saved in the encrypted
   persistent storage.

   For the time being, we recommend you to only activate the **Personal
   Data** persistence feature. You can modify the configuration of the
   encrypted persistent storage later on according to your needs.

   <div class="caution">
   <p>As a general rule, only activate the persistence features that you
   need, as you might otherwise store more data than intended or weaken
   your anonymity.</p>
   </div>

   Refer to our documentation on [[configuring the encrypted persistent
   storage|doc/first_steps/persistence/configure]] to learn more about
   the other persistence features.

5. Click **Save**.

   <div class="step-image">[[!img inc/infography/restart-on-tails.png link="no"]]</div>

1. Shut down the computer and restart on <span class="windows-usb linux-usb mac-usb">the final</span><span class="clone">the new</span><span class="debian-usb expert dvd">the</span> Tails USB stick.

1. In *Tails Greeter*:

   - Select your preferred language in the drop-down list on the bottom left of the screen.
   - In the <span class="guilabel">Use persistence?</span> section, choose <span class="guilabel">Yes</span> to
enable the encrypted persistent storage for the current working session.
   - Enter the passphrase of the persistent volume in the
<span class="guilabel">Passphrase</span> text box.</span>
   - If you select the <span class="guilabel">Read-Only</span> check box, the
content of encrypted persistent storage is available and you can modify
it but the changes are not be saved.
   - Click **Login**.

   <div class="caution">
   <p>Use the encrypted persistent storage only when necessary. You can
   always start Tails without activating the encrypted persistent
   storage.</p>
   </div>

1. After 15&ndash;30 seconds, the Tails desktop appears.

1. You can now save your personal files and working documents in the
**Persistent** folder. To open the **Persistent** folder choose
<span class="menuchoice">
  <span class="guimenu">Places</span>&nbsp;▸
  <span class="guimenuitem">Persistent</span></span>.

<div class="state-image debian-usb expert">[[!img inc/infography/tails-usb-with-persistence.png link="no"]]</div>
<div class="state-image windows-usb linux-usb mac-usb clone">[[!img inc/infography/final-tails-with-persistence.png link="no"]]</div>
<div class="state-image dvd">[[!img inc/infography/tails-usb-with-persistence.png link="no"]]</div>

<div class="trophy" id="recommendations">

<p>You now have a working Tails, congrats!</p>

<h3>Final recommendations</h3>

<p>It is very important to keep your Tails up-to-date, otherwise
it will become vulnerable to numerous security issues.</p>

<p>To be notified of new versions and important project news, follow our
[[news feed|news]] or subscribe to our
<a href="https://mailman.boum.org/listinfo/amnesia-news">newsletter</a>
(one or two messages per month):</p>

<p>
<form method="post" action="https://mailman.boum.org/subscribe/amnesia-news" target="_blank">
	<input class="text" name="email" value=""/>
	<input class="button" type="submit" value="Subscribe"/>
</form>
</p>

<p>Also remember
that Tails does not protect from everything. Have a look at our
[[warnings|doc/about/warning]].</p>

<p>If you face any problem, use the <span class="guilabel">Report an error</span> launcher on the Tails desktop
or visit our [[support pages|support]].</p>

<p>We hope you enjoy using Tails :)</p>

</div>
